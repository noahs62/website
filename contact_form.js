const form = document.querySelector('.contact_form');
const submit_input = form.querySelector('button[type="submit"]');

var _name, email, telnum, message;

function send_email() {
    Email.send({
        SecureToken : "fd78b08c-d5f6-4e3f-83f3-d2846606bfea",
        To : 'noah@noahsw.xyz',
        From : "internal@noahsw.xyz",
        Subject : `${_name} - contact form`,
        Body : `<b>Name:</b> ${_name}<br>
        <b>Email:</b> ${email}<br>
        <b>Tel num:</b> ${telnum}<br>
        <b>Message:</b><br>
        ${message}<br>
        <b>---</b>`
    }).then(
        alert("SUCCESS\n\nThanks. Ill get back to you when I can.")
    );

}

function get_data(e) {
    e.preventDefault();
    var form_data = new FormData(form);

    _name = form_data.get('name');
    email = form_data.get('email');
    telnum = form_data.get('number');
    message = form_data.get('message');

    message = message.replace(/(?:\r\n|\r|\n)/g, '<br>');

    if (_name != "" && email != "" && message != "") {
        send_email();
    } else {
        alert("Please fill out all required fields.");
    }
}

document.addEventListener('DOMContentLoaded', function(){
    submit_input.addEventListener('click', get_data, false);
}, false);
